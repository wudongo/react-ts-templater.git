# React app Template
<h2 align="left">
  <a href="https://gitee.com/biaovorg/project-template/tree/vue"><img src="https://img.shields.io/badge/version-v1.0.0-g" /></a>
</h2>  

## 项目介绍
技术栈为React18、TypeScript、styled-components 等  
## 使用方式

### 依赖安装
```Javascript
yarn
```
### 开发环境调试
```Javascript
yarn dev
```