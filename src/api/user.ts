import { postRequest } from './base';

const login: FetchFunction<User> = postRequest('/user/login');

export {
    login,
}