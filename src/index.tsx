import React from 'react';
import ReactDOM from 'react-dom/client';

import './index.less';
import "@arco-design/web-react/dist/css/arco.css";

import App from './page/app';

const container = document.getElementById('root') as HTMLElement;
const root = ReactDOM.createRoot(container);

root.render(<App />);