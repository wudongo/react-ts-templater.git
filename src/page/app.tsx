import React from "react";
import styled from 'styled-components';
import { BrowserRouter } from 'react-router-dom'

import Router from '@/components/Router/Router';
import HeadBar from "@/components/HeadBar/HeadBar";


interface Props {

};

const StyledAPP = styled.div`
    position: relative;
    width: 100vw;
    height: 100vh;
    display: flex;
    flex-direction: column;

    div[data-type='mainpage'] {
        position: relative;
        width: 100%;
        height: calc(100vh - ${(props) => (props?.barHeight || 48)}px);
        box-sizing: border-box;
        padding: 16px 8px 16px 16px;
        overflow-y: scroll;
    }

    div::-webkit-scrollbar {
        width: 8px;
    }
    div::-webkit-scrollbar-thumb {
        border-radius: 4px;
        -webkit-box-shadow: inset 0 0 12px rgba(25, 25, 25, 0.941);
    }
    div::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 15px rgba(25, 25, 25, 0.2);
        border-radius: 0;
    }
`;

const App = (_props: Props) => {

    const barHeight = 48;

    return (
        <BrowserRouter>
            <StyledAPP barHeight={barHeight}>
                <HeadBar barHeight={barHeight}/>
                <Router />
            </StyledAPP>
        </BrowserRouter>
    )
}


export default App;