interface ResponseData <T=any> {
    ResponseMetadata: {
        Action: string
        Error?: {
            Code: number;
            Msg: string;
        }
    };
    Result: any
};

type FetchFunction <T = any> = (args?: any) => Promise<T>; 