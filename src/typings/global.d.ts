declare module '*.svg';
declare module '*.png';
declare module '*.jpg';
declare module '*.jpeg';
declare module '*.gif';
declare module '*.bmp';
declare module '*.tiff';

declare module '*.less' {
  const resource: { [key: string]: string };
  export = resource;
}

interface BaseProps extends React.InputHTMLAttributes<Omit<HTMLDivElement, 'onClick'>> {
  onClick?: () => void | typeof HTMLDivElement.onClick;
}

interface BaseResponse extends Omit<T, 'id | create_time'> {
  id: number;
  create_time: number;
};