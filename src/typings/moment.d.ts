interface IComment extends BaseResponse {
    name: string;
    avatar: string;
    thumb_num: number;
    content: string;
}
interface Moment extends BaseResponse {
    title: string;
    creator: 'Banana' | 'Carrot';
    favorite_num: number;
    content: string;
    comments: IComment[];
};