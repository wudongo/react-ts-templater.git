import React from "react";
import { useNavigate } from "react-router";
import { StyledBar } from "./styled";
interface IHeadBar {
    barHeight: string | number;
};

const HeadBar = (props: Partial<IHeadBar>) => {

    const navigate = useNavigate();
    const to = (url: string) => () => navigate(url);

    return (
        <StyledBar barHeight={props?.barHeight}>
            <div className="barContainer">
            </div>
        </StyledBar>
    )
}


export default HeadBar;