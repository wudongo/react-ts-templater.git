import styled from 'styled-components';

export const StyledBar = styled.div`
position: relative;
width: 100%;
height: ${(props) => Number(props?.barHeight || 48)}px;
display: flex;
flex-direction: row;
justify-content: space-between;
align-items: center;
background: #1d2129;
box-sizing: border-box;
padding: 0px 24px;
will-change: transform;
z-index: 99999;

.barContainer {
    width: 100%;
    position: relative;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    font-size: 20px;
    font-weight: bold;
    color: white;

    .bar-content {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        align-items: center;
    }
}
`;