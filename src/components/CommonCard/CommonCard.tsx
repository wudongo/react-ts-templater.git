import React, { ReactElement } from "react";
import { StyledCard } from "./styled";

interface ICommonCard {
    title?: string;
    titleNode?: ReactElement;
    children?: any;
    titleStyle?: Record<string, string>;
    childrenStyle?: Record<string, string>;
}

const CommonCard = (props: ICommonCard & BaseProps) => {

    const { title, children, ...rest } = props;

    return (
        <StyledCard {...rest} className={props?.className || ''} style={props?.style || {}}>
            <div className="title" style={props?.titleStyle || {}}>{ props?.titleNode ?? title }</div>
            <div className="children" style={props?.childrenStyle || {}}>
                { children }
            </div>
        </StyledCard>
    )
}


export default CommonCard;