
import styled from 'styled-components';

export const StyledCard = styled.div`
    position: relative;
    width: 100%;
    height: 100%;
    margin-right: 16px;

    .title {
        background: white;
        border-top-left-radius: 6px;
        border-top-right-radius: 6px;
        height: 48px;
        font-size: 18px;
        color: black;
        font-weight: bold;
        display: flex;
        opacity: 0.95;
        flex-direction: row;
        padding: 0px 16px;
        align-items: center;
    }

    .children {
        position: relative;
        height: calc(100% - 48px);
        padding: 0px 16px;
        background: white;
        opacity: 0.6;
        border-bottom-left-radius: 6px;
        border-bottom-right-radius: 6px;
        box-sizing: border-box;
        padding: 16px 16px;
        overflow: hidden;
        word-break: break-all;
    }
`;