import { Spin } from '@arco-design/web-react';
import React, { lazy, Suspense, useEffect } from 'react';
import styled from 'styled-components';
import { Routes, Route, useNavigate } from 'react-router-dom'
 
const Template = lazy(()=>import('@/page/Template/Template'));

const StyledSpin = styled(Spin)`
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
`;

const Redirect = ({ to }) => {

    const navigate = useNavigate();

    useEffect(() => {

      navigate(to);
      
    });

    return null;
}

export interface IRouterConfig {
    config?: Record<string, any>;
}

const RouterConfig = (props: IRouterConfig) => {


    return (
        <>
            <Suspense fallback={
                <StyledSpin dot ></StyledSpin>
            }>
            <Routes>
                <Route path="/" element={<Redirect to={"/center"}/>} />
                <Route path="/center" element={<Template />} />
            </Routes>
            </Suspense>
        </>
    )
}


export default RouterConfig;